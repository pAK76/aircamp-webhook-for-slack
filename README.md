# README #

This is an example Aircamp webhook for posting task events into a desired slack channel

### Quick overview ###

Currently this webhook processed only three types of events:

* `TASK_TRANSITION` — change of task status. May also contain any other field changes if your workflow requires.
* `TASK_COMMENT_ADD` — new comment on that task
* `TASK_CREATE` — new task in that project

Slack token and channel name are stored in `config/local.php`. It is not in repository, so you should copy `config/example.php` and fill in the fields.

### Tips on extending ###

If you want to fork/modify this webhook, note the following:

* Aircamp API always want to receive `HTTP 200 OK` in response. Use 4xx or 5xx only if you really receive something invalid in `POST` body.
* JSON format Webhook receives is exactly the same as in Activity Stream API (check `/activity` page of your Aircamp installation). 
* API uses all POST body to send JSON about task events. If you want to pass some other parameters like channel name or events to handle, use query string and draw them from `$_GET`.
* This webhook doesn't store slack token in repository. Please, continue this good practice and store all private settings in `config/local.php`. Don't forget to update example.php so others can rebuild it.

### Dependencies ###

This webhook uses [php-slack](https://github.com/Frlnc/php-slack), yet slightly modified. The only change is removed message escaping so we can use annotated URLs. Otherwise `<` and `>` symbols will be html-encoded. Take a note: this puts responsibility to escape `<`, `>` and `&` symbols on your shoulders. 
<?php
	error_reporting(E_ALL);
	ini_set('display_errors', '1');

	require_once 'config/local.php';

	_require_all('frlncSlack');

	use Frlnc\Slack\Http\SlackResponseFactory;
	use Frlnc\Slack\Http\CurlInteractor;
	use Frlnc\Slack\Core\Commander;

	$headers = getallheaders();
	if ($headers['Content-Type'] != 'application/json') {
		http_response_code(400);
		die('INVALID Content-Type');
	}
	$body = file_get_contents('php://input');
	$bodyObject = json_decode($body, true);
	if(!$bodyObject) {
		http_response_code(400);
		die('Broken JSON');
	}

	$actionType = $bodyObject["field_name"];

	$user = $bodyObject["user"];
	$firstName = $user["first_name"];
	$lastName = $user["second_name"];

	$task = $bodyObject["task"];
	$taskId = $task["id"];
	$taskName = $task["name"];
	$taskNameLength = strlen(utf8_decode($taskName));
	if ($taskNameLength > 64) {
		$taskName = mb_substr($taskName, 0, 64, 'UTF-8')."…";
	}
	$project = $task["project"];
	$projectShortName = $project["short_name"];

	if ($actionType == "TASK_TRANSITION") {
		foreach ($bodyObject["subactions"] as $subaction) {
			if ($subaction["field_name"] == "WORKFLOW_STATUS") {
				$oldValue = $subaction["old_value"];
				$newValue = $subaction["new_value"];
				break;
			}
		}
		$oldStatus = $oldValue["name"];
		$newStatus = $newValue["name"];
		$message = sprintf("*%s %s* совершил переход в задаче <%s/view/%s/%d|%s-%d «%s»> из статуса *%s* в статус *%s*",
			Commander::format($firstName),
			Commander::format($lastName),
			Config::AIRCAMP_HOME,
			Commander::format($projectShortName),
			Commander::format($taskId),
			Commander::format($projectShortName),
			Commander::format($taskId),
			Commander::format($taskName),
			Commander::format($oldStatus),
			Commander::format($newStatus)
		);
	} elseif ($actionType == "TASK_COMMENT_ADD") {
		$comment = $bodyObject["new_value"];
		if ($comment > 128) {
			$comment = mb_substr($taskName, 0, 128, 'UTF-8')."…";
		}
		$message = sprintf("*%s %s* добавил комментарий\n>%s\nк задаче <%s/view/%s/%d|%s-%d «%s»>",
			Commander::format($firstName),
			Commander::format($lastName),
			Commander::format($comment),
			Config::AIRCAMP_HOME,
			Commander::format($projectShortName),
			Commander::format($taskId),
			Commander::format($projectShortName),
			Commander::format($taskId),
			Commander::format($taskName)
		);
	} elseif ($actionType == "TASK_CREATE") {
		$message = sprintf("*%s %s* создал задачу <%s/view/%s/%d|%s-%d «%s»>",
			Commander::format($firstName),
			Commander::format($lastName),
			Config::AIRCAMP_HOME,
			Commander::format($projectShortName),
			Commander::format($taskId),
			Commander::format($projectShortName),
			Commander::format($taskId),
			Commander::format($taskName)
		);
	} else {
		die('Action type ' . $actionType . ' ignored');
	}

	/**
     * Scan the api path, recursively including all PHP files
     *
     * @param string  $dir
     * @param int     $depth (optional)
     */
    function _require_all($dir, $depth=0) {
        if ($depth > 6) {
            return;
        }
        // require all php files
        $scan = glob("$dir/*");
        foreach ($scan as $path) {
            if (preg_match('/\.php$/', $path)) {
                require_once $path;
            }
            elseif (is_dir($path)) {
                _require_all($path, $depth+1);
            }
        }
    }

	$interactor = new CurlInteractor;
	$interactor->setResponseFactory(new SlackResponseFactory);

	$commander = new Commander(Config::SLACK_TOKEN, $interactor);

	$response = $commander->execute('chat.postMessage', [
	    'channel' => Config::SLACK_CHANNEL,
	    'text'    => $message
	]);

	if ($response->getStatusCode() == 200)
	{
		die('All Ok!');
	}
	else
	{
		die('Something wrong');
	}
?>

<?php
  class Config
  {
    const SLACK_TOKEN = "YOUR_SLACK_TOKEN";
    const SLACK_CHANNEL = "YOUR_SLACK_CHANNEL";
    const AIRCAMP_HOME = "YOUR_AIRCAMP_HOME_PAGE";
  }
?>